package com.zefilosz.mysocket

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var socket : Socket
    private val url = "http://27.254.137.205:9118"
    //private val url = "https://socket-io-chat.now.sh/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        socket = IO.socket(url)

        button_test.setOnClickListener {
            socket.emit("chat.post", "test")
            //socket.emit("add user", "Art")
        }

        socket.on("chat.response", onNewListen)
        //socket.on("login", onLogin)
        socket.connect()
    }

    override fun onDestroy() {
        super.onDestroy()

        socket.disconnect()
        socket.off("chat.response", onNewListen)
        //socket.off("login", onLogin)
    }

    private val onNewListen = Emitter.Listener { args ->
        runOnUiThread {
            val data = args[0] as JSONObject
            var status : String = data.getString("status")
            var message : String = data.getString("message")
            textView.text = status + " | " + message
        }
    }

    private val onLogin = Emitter.Listener { args ->
        runOnUiThread {
            val data = args[0] as JSONObject
            var numUsers : Int = data.getInt("numUsers")
            textView.text = numUsers.toString()
        }
    }
}
